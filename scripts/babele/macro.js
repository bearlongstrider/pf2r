import { ruModule } from "../init.js";
import { hash } from "../utils.js";

export function macroCodeSubstitution(d, t) {
    const setting = game.settings.get(ruModule, "macroReplacements_experimental");
    if (setting === "off" || !t)
        return d;

    const h = hash(d);
    if (hash(d) != t.hash && setting === "safe") {
        ui.notifications.warn(`Несовпадение хеша кода макроса. Ожидается ${t.hash}, найден ${h} Замена макроса не произведена.`)
        return d;
    }

    for (const r of t.replace) {
        let { pattern, value, many, regex } = r;
        if(regex)
            pattern = many ? new RegExp(pattern, "g") : new RegExp(pattern);
        d = many ? d.replaceAll(pattern, value) : d.replace(pattern, value);
    }

    return d;
}

