import { nameConverter } from "./babele.js";
import { ruModule } from "../init.js";

// Не уверен что можно легко определить, какой журнал сейчас обрабатывается, поэтому здесь список страниц. Если эта страница неходится в журнале, то мы сортируем его. 
const journalSortingWhitelist = [
    "Jkl5KOZih7MoMqAJ", // Архетипы (Алхемические архетипы)
    "qMS6QepvY7UQQjcr", // Домены (Мерзость)
    "Om4e4tsgNWrComie", // Классы (Алхимик)
    "u6IGUd8Gtarj6h7K", // Родословные (Универсальные родословные)
    "quxPxuMub8k6abzN"  // Колода очков героизма (Мощь предков)
]

export function journalPagesConverter(data, translation) {
    if (!translation)
        return data;
    const showEdited = game.settings.get(ruModule, "babeleShowEdited");
    const keepOriginalName = game.settings.get(ruModule, "babeleJournalPageKeepOriginalName");
    const sortPages = game.settings.get(ruModule, "babeleJournalSortPages") && data.some(d => journalSortingWhitelist.includes(d._id));

    let toSort = [];

    let result = data.map(e => {
        let result = { ...e };
        const t = translation[e.name];
        if (!t) {
            if (sortPages)
                toSort.push(({ id: result._id, name: result.name }));
            return result;
        }
        result.name = nameConverter(e.name, t.name, showEdited, keepOriginalName);
        result.text.content = t.text ?? e.text.content;

        if (sortPages)
            toSort.push(({ id: result._id, name: result.name, customSort: t.sort }));
        return result;
    });

    if (!sortPages)
        return result;

    toSort = toSort.sort((a, b) => a.name.localeCompare(b.name));
    toSort.forEach((e, i) => {
        const s = e.customSort;
        if (!s) {
            e.sort = i;
        }
        else {
            e.sort = i + Number(s);
        }
    });

    result.forEach(e => {
        e.sort = toSort.find(s => s.id == e._id).sort;
    })

    return result;
}