import { ruModule } from "../init.js";

export function highlightUuidRarity() {
    if (game.settings.get(ruModule, "inlineLinkRarity"))
        libWrapper?.register(ruModule,
            "TextEditor._createContentLink",
            async (wrapped, match, params) => {
                console.log(match, params);
                const link = await wrapped(match, params);
                const uuid = link.attributes['data-uuid']?.value;
                if (uuid) {
                    const item = await fromUuid(uuid);
                    const rarity = item?.system?.traits?.rarity;
                    if (rarity == 'uncommon') {
                        link.classList.add('pf2e-ru-inline-uncommon');
                    }
                    else if (rarity == 'rare') {
                        link.classList.add('pf2e-ru-inline-rare');
                    }
                    else if (rarity == 'unique') {
                        link.classList.add('pf2e-ru-inline-unique');
                    }
                }
                console.log(link);
                return Promise.resolve(link);
            },
            "MIXED");
}