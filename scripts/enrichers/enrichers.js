import { addUnitEnricher } from "./units.js";
import { modifyInlineLinkEnricher } from "./inline-links.js";
import { highlightUuidRarity } from "./uuid.js";

export function addTextEnrichers(){
    addTraitConditionEnricher();
    addUnitEnricher();

    modifyInlineLinkEnricher();
    highlightUuidRarity();
}

function addTraitConditionEnricher() {
    CONFIG.TextEditor.enrichers.push({
        pattern: new RegExp(/@(Trait|Condition)\[([^\]]+)\](?:{([^}]+)})/, 'g'),
        enricher: (match, options) => {
            const [m, tag, name, label] = match
            const slug = game.pf2e.system.sluggify(name);
            const element = document.createElement('span');
            element.innerHTML = label;
            switch (tag) {
                case "Condition":
                    element.classList.add('pf2e-ru-inline-condition');
                    break;
                case "Trait":
                    element.classList.add("pf2e-ru-inline-trait");
                    const desc = CONFIG.PF2E.traitsDescriptions[slug] ??
                        Object.entries(CONFIG.PF2E.traitsDescriptions).find(([k, _]) => k.startsWith(slug))?.[1];
                    if (desc) {
                        element.setAttribute('data-tooltip-class', "pf2e");
                        element.setAttribute('data-tooltip', desc);
                    }
                    else {
                        console.warn(`Обогатитель текста | Неизвестный признак: ${name}`)
                    }
                    break;
                default:
                    return m;
            }
            return element;
        }
    })
}
