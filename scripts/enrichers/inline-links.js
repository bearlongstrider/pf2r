import { ruModule } from "../init.js";

const settings = { labels: "off" };

const damageType = {
    "acid": "{} урона кислотой",
    "cold": "{} урона холодом",
    "electricity": "{} урона электричеством",
    "fire": "{} урона огнём",
    "force": "{} урона силой",
    "sonic": "{} урона звуком",
    "vitality": "{} урона жизненностью",
    "void": "{} урона пустотой",
    "bleed": "{} урона кровотечением",
    "bludgeoning": "{} дробящего урона",
    "piercing": "{} колющего урона",
    "slashing": "{} рубящего урона",
    "mental": "{} ментального урона",
    "poison": "{} урона ядом",
    "spirit": "{} духовного урона",
    "untyped": "{} урона",
    "precision": "{} точного урона",
}
const splashType = {
    "acid": "{} урона кислотными брызгами",
    "cold": "{} урона холодными брызгами",
    "electricity": "{} урона электрическими брызгами",
    "fire": "{} урона огненными брызгами",
    "force": "{} урона силовыми брызгами",
    "sonic": "{} урона звуковыми брызгами",
    "vitality": "{} урона брызгами жизненности",
    "void": "{} урона брызгами пустоты",
    "bludgeoning": "{} урона дробящими брызгами",
    "piercing": "{} урона колющими брызгами",
    "slashing": "{} урона рубящими брызгами",
    "mental": "{} ментального урона брызгами",
    "poison": "{} урона ядовитыми брызгами",
    "spirit": "{} духовного урона брызгами",
    "untyped": "{} урона брызгами"
}

const checkType = {
    basic: "простой {}",
    
    fortitude: "спасбросок Стойкости",
    reflex: "спасбросок Рефлекса",
    will: "спасбросок Воли",
    acrobatics: "проверка Акробатики",
    arcana: "проверка Арканы",
    athletics: "проверка Атлетики",
    crafting: "проверка Ремесла",
    deception: "проверка Обмана",
    diplomacy: "проверка Дипломатии",
    intimidation: "проверка Запугивания",
    medicine: "проверка Медицины",
    nature: "проверка Природы",
    occultism: "проверка Оккультизма",
    performance: "проверка Выступления",
    religion: "проверка Религии",
    society: "проверка Общества",
    stealth: "проверка Скрытности",
    survival: "проверка Выживания",
    thievery: "проверка Воровства",

    flat: "чистая проверка",

    computers: "проверка Компьютеров",
    piloting: "проверка Пилотирования"
}

const templateType = {
    cone: "{}-футовый конус",
    line: "{}-футовая линия",
    emanation: "{}-футовая эманация",
    burst: "{}-футовый взрыв",
    square: "{}-футовый квадрат",
    width: "шириной в {} футов"
}

class Damage {
    constructor(formula, modifier) {
        this.formula = formula;
        this.type = "untyped";
        this.modifier = undefined;
        if (modifier && modifier != "damage,healing") {
            const parts = modifier.split(",");
            for (const p of parts) {
                if (Object.keys(damageType).includes(p)) {
                    this.type = p;
                } else if (["healing", "persistent", "splash"].includes(p)) {
                    this.modifier = p;
                }
            }
        }
    }

    applyModifier(modifier) {
        if (modifier == "damage,healing") {
            this.type = "untyped";
            this.modifier = undefined;
        }
        else {
            const parts = modifier.split(",");
            for (const p of parts) {
                if (Object.keys(damageType).includes(p)) {
                    this.type = p;
                } else if (["healing", "persistent", "splash"].includes(p)) {
                    this.modifier = p;
                }
            }
        }
    }

    get label() {
        const f = this.formula.replaceAll("*", " * ")
        switch (this.modifier) {
            case "splash":
                return splashType[this.type].replace('{}', f);
                break;
            case "persistent":
                return damageType[this.type].replace('{}', `${f} продолжительного`);
                break;
            case "healing":
                return "{} ОЗ".replace("{}", f);
            default:
                return damageType[this.type].replace('{}', f);
        }
    }
}

function seekEnd(text, h = "(", l = ")") {
    let level = 0;
    for (let i = 0; i < text.length; ++i) {
        const c = text[i];
        if (c == h)
            level += 1;
        else if (c == l) {
            level -= 1;
            if (level == 0)
                return i;
        }
    }
    return null;
}

const formulaPattern = /^((?:\d+\*)?-?(?:\d+d\d+|\d+))(?:\[([^\]]+)\])?/;
const modifierPattern = /^\[([^\]]+)\]/;

function buildDamages(formula) {
    const damages = []
    while (formula.length > 0) {
        if (formula[0] == "," || formula[0] == "+") {
            formula = formula.slice(1);
        }
        else if (formula[0] == "(") {
            const groupEnd = seekEnd(formula);
            const inner = buildDamages(formula.slice(1, groupEnd));
            formula = formula.slice(groupEnd + 1);
            const m = formula.match(modifierPattern);
            if (m) {
                inner.forEach(d => d.applyModifier(m[1]));
                formula = formula.slice(m[0].length)
            }
            damages.push(...inner);
            continue;
        }
        else {
            const m = formula.match(formulaPattern);
            if (m) {
                formula = formula.slice(m[0].length);
                const d = new Damage(m[1], m[2]);
                if (damages.length > 0) {
                    const prevDamage = damages[damages.length - 1];
                    if (prevDamage.type == d.type && prevDamage.modifier == d.modifier) {
                        if (d.formula[0] == "-") {
                            prevDamage.formula += " − " + d.formula.slice(1);
                        }
                        else {
                            prevDamage.formula += " + " + d.formula;
                        }
                        continue;
                    }
                }
                damages.push(d);
                continue;
            }
            else {
                // Что-то непонятное в формуле. Возвращаем как было
                return [];
            }
        }
    }
    return damages;
}

const nowrap = (text, pad) => `<span style="white-space:nowrap;${pad ? "padding-right:4px" : ""}">${text}</span>`;

function modifyDamage(elementPromise, match) {
    return elementPromise.then(r => {
        let formula = r.getAttribute("data-formula");
        if (!formula || match[2].includes("shortLabel"))
            return r;

        if (match[3] && settings.labels !== "style")
            return r;

        let newLabel = "";
        let parts = [];
        let separators = [];
        const icon = getLinkIcon(r);
        if (match[3]) {
            let oldLabel = getLabel(r).innerHTML;
            const sep = /(,|\sи)\s/;
            parts = [];
            while (oldLabel.length > 0) {
                const m = oldLabel.match(sep);
                if (m) {
                    separators.push(m[1]);
                    parts.push(oldLabel.slice(0, m.index));
                    oldLabel = oldLabel.slice(m.index + m[0].length);
                } else {
                    parts.push(oldLabel);
                    break;
                }
            }
        }
        else {
            formula = formula.replaceAll(/[\s\{\}]/g, "");
            const damages = buildDamages(formula);
            if (damages.length == 0) {
                return r;
            }
            parts = damages.map(d => d.label);
            separators = parts.length > 2 ? Array(parts.length - 2).fill(",") : [];
            separators.push(" и");
        }
        parts[0] = icon.outerHTML + parts[0];
        parts[parts.length - 1] = nowrap(parts[parts.length - 1]);
        if (parts.length > 1) {
            parts[parts.length - 2] = nowrap(parts[parts.length - 2] + separators[separators.length - 1]);
        }
        for (let i = 0; i < parts.length - 2; ++i) {
            parts[i] = nowrap(parts[i] + separators[i]);
        }
        newLabel = parts.join(" ");

        r.innerHTML = `<span>${newLabel}</span>`;

        if (settings.labels == "style")
            r.classList.add("pf2e-ru-inline-wrappable")
        return r;
    })
}

function modifyCheck(elementPromise, match) {
    return elementPromise.then(r => {
        const type = match[2].match(/^(?:type:)?([^\]\|]+)/)?.[1];
        if ((match[3] && settings.labels !== "style") || !type || !checkType[type])
            return r;
        let newLabel = "";
        const icon = getLinkIcon(r);
        const dc = getDC(r);
        const oldLabel = getLabel(r).innerHTML.replace(dc?.outerHTML ?? "", "").trim();
        if (dc)
            dc.innerHTML = "с " + dc.innerHTML;

        if (!match[3]) {
            newLabel = match[2].includes("|basic") ? checkType.basic.replace("{}", checkType[type]) : checkType[type];
            newLabel = newLabel[0].toLocaleUpperCase() + newLabel.slice(1);
        }
        else {
            newLabel = oldLabel;
        }

        const parts = newLabel.split(" ");
        parts[0] = parts[0];
        for (let i = 0; i < parts.length - 1; ++i) {
            parts[i] = nowrap(parts[i], true);
        }
        parts[parts.length - 1] = nowrap(parts[parts.length - 1] + (dc?.outerHTML ? " " + dc.outerHTML : ""));

        newLabel = parts.join("");
        r.innerHTML = icon.outerHTML + `<span class="label">${newLabel}</span>`;

        if (settings.labels == "style" && false)
            r.classList.add("pf2e-ru-inline-wrappable")
        return r;
    });
}


function modifyTemplate(elementPromise, match) {
    return elementPromise.then(r => {
        const type = match[2].match(/^(?:type:)?([^\]\|]+)/)?.[1];
        if ((match[3] && settings.labels !== "style") || !type || !templateType[type])
            return r;
        let newLabel = "";
        const oldLabel = r.innerHTML;

        if (!match[3]) {
            const distance = match[2].match(/distance:(\d+)/)?.[1];
            if (typeof distance === 'undefined') return r;
            const width = match[2].match(/width:(\d+)/)?.[1];
            newLabel = nowrap(templateType[type].replace("{}", distance), !!width);
            if (typeof width === 'string')
                newLabel = `${newLabel}${nowrap(templateType.width.replace("{}", width))}`;
        }
        else {
            newLabel = oldLabel;
        }
        r.innerHTML = newLabel;

        if (settings.labels == "style")
            r.classList.add("pf2e-ru-inline-wrappable")
        return r;
    });
}


export function modifyInlineLinkEnricher() {
    settings.labels = game.settings.get(ruModule, 'inlineLinkLabels_experimental');
    if (settings.labels !== "off")
        libWrapper?.register(ruModule,
            "game.pf2e.TextEditor.enrichString",
            (wrapped, match, options) => {
                const res = wrapped(match, options);

                if (match[1] == "Damage") {
                    return modifyDamage(res, match);
                }
                else if (match[1] == "Check") {
                    return modifyCheck(res, match);
                }
                else if (match[1] == "Template") {
                    return modifyTemplate(res, match);
                }
                return res;
            },
            "MIXED");
}

function getLinkIcon(element) {
    return element.querySelector(".fa-solid");
}

function getLabel(element) {
    return element.querySelector("span");
}
function getDC(element) {
    return element.querySelector("[data-visibility=gm]") ?? element.querySelector("[data-visibility=all]")
}