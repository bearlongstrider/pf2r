const numberPrecision = 2;

const unitData = {
    units: {
        "foot": {
            "group": "L",
            "unit": "m",
            "scale": 0.3048
        },
        "mile": {
            "group": "L",
            "unit": "km",
            "scale": 1.60934
        },
        "square-inch": {
            "group": "L2",
            "unit": "cm2",
            "scale": 6.4516
        },
        "inch": {
            "group": "L",
            "unit": "cm",
            "scale": 2.54
        },
        "gallon": {
            "group": "L3",
            "unit": "liter",
            "scale": 3.78541
        },
        "cubic-foot": {
            "group": "L3",
            "unit": "liter",
            "scale": 28.316846592
        },
        "cubic-inch": {
            "group": "L3",
            "unit": "cm3",
            "scale": 16.3871
        },
        "pint": {
            "group": "L3",
            "unit": "liter",
            "scale": 0.568261
        },
        "pound": {
            "group": "M",
            "unit": "kg",
            "scale": 0.453592
        },
        "ounce": {
            "group": "M",
            "unit": "g",
            "scale": 28.3495
        },
        "fahrenheit": {
            "group": "W",
            "unit": "celsius",
            "scale": 0.55555555555555555555555555555556,
            "offset": -32
        }
    },
    scales: {
        "L": {
            "m": 1,
            "cm": 0.01,
            "km": 1000
        },
        "L2": {
            "cm2": 0.0001,
            "m2": 1
        },
        "L3": {
            "liter": 1,
            "cm3": 0.001,
            "m3": 1000
        },
        "M": {
            "g": 1,
            "kg": 1000,
            "t": 1000000
        },
        "W": {
            "celsius": 1
        }
    },
    labels: {
        "km": "км",
        "m": "м",
        "cm": "см",
        "cm2": "см<sup>2</sup>",
        "m2": "м<sup>2</sup>",
        "liter": "л",
        "cm3": "см<sup>3</sup>",
        "m3": "м<sup>3</sup>",
        "kg": "кг",
        "g": "г",
        "t": "т",
        "celsius": "℃"
    }
}

export function addUnitEnricher() {
    CONFIG.TextEditor.enrichers.push({
        pattern: new RegExp(/@Unit\[([^\]\|]+)\|([^\]]+)\](?:{([^}]+)})/, 'g'),
        enricher: (match, options) => {
            const [m, unit, value, label] = match
            const element = document.createElement('span');
            element.innerHTML = label;
            try {
                const tooltip = unitTooltip(unit, value);
                element.classList.add('pf2e-ru-inline-unit');
                element.setAttribute('data-tooltip-class', "pf2e");
                element.setAttribute('data-tooltip', tooltip);
                return element;
            }
            catch {
                return element;
            }
        }
    })
}

const truncateNumber = (amount) => Number((amount).toPrecision(numberPrecision))

function scaleTo(value, unitFrom, unitTo, scales) {
    return value / scales[unitFrom] * scales[unitTo];
}

function optimalScale(value, unit, scales) {
    const normalized = value * scales[unit];
    const scaled = Object.entries(scales).map(([k, v]) => ({ unit: k, value: normalized / v })).sort((a, b) => a.value - b.value);
    const result = scaled.filter(({ value }) => value > 0.5)[0] ?? scaled[scaled.length - 1];
    return [result.value, result.unit];
}

function unitTooltip(unitLabel, valuesText) {

    const originalUnit = game.pf2e.system.sluggify(unitLabel);
    const converter = unitData.units[originalUnit];
    if (!converter) {
        ui.notifications.warn(`Неизвестная единица измерения: ${unitLabel}`);
        return null;
    }
    const scales = unitData.scales[converter.group];
    const values = valuesText.split('-').map(v => Number(v));
    let valueFrom = (values[0] + (converter.offset ?? 0)) * converter.scale;
    let optScale;
    [valueFrom, optScale] = optimalScale(valueFrom, converter.unit, scales);
    let valueTo = values[1] ? scaleTo((values[1] + (converter.offset ?? 0)) * converter.scale, converter.unit, optScale, scales) : null;
    const label = unitData.labels[optScale];
    return valueTo ? `~${truncateNumber(valueFrom)}–${truncateNumber(valueTo)} ${label}` : `~${truncateNumber(valueFrom)} ${label}`

}